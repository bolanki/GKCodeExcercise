﻿namespace Code.Exercise.April2018.Models
{
  public class MessageData
  {
    public string RecipientName { get; set; }
    public string RecipientAddress { get; set; }
    public string ActionUri { get; set; }
  }
}
