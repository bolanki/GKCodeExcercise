﻿using System.Threading.Tasks;
using Code.Exercise.April2018.Core.Interfaces;

namespace Code.Exercise.April2018.Core
{
  public class ActivationHelper : IActivationHelper
  {
    public Task<string> GetActionUri(string contactHash)
    {
      return Task.FromResult($"https://www.test.com/activate/{contactHash}");
    }
  }
}
