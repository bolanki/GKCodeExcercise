﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Code.Exercise.April2018.Models;

namespace Code.Exercise.April2018.Core.Interfaces
{
  public interface IUserService
  {
    Task<IEnumerable<MessageData>> GetContactEmailParamsAsync(IEnumerable<Contact> collection);
  }
}
