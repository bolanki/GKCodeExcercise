﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Code.Exercise.April2018.Core.Interfaces
{
  public interface IDataContext
  {
    Task<List<string>> GetContactHashes();
  }
}
