﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Code.Exercise.April2018.Core.Interfaces;

namespace Code.Exercise.April2018.Tests
{
  public class FakeDataContext : IDataContext
  {
    private List<string> _hashList;

    public FakeDataContext(List<string> hashList)
    {
      _hashList = hashList;
    }

    public Task<List<string>> GetContactHashes()
    {
      return Task.FromResult(_hashList);
    }

    public void SetHashList(List<string> hashes)
    {
      _hashList = hashes;
    }
  }
}
