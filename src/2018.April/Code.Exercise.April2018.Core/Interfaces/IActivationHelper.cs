﻿using System.Threading.Tasks;

namespace Code.Exercise.April2018.Core.Interfaces
{
  public interface IActivationHelper
  {
    Task<string> GetActionUri(string contactHash);
  }
}
