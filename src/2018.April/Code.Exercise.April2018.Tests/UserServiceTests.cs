﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Code.Exercise.April2018.Core;
using Code.Exercise.April2018.Core.Interfaces;
using Code.Exercise.April2018.Models;
using FizzWare.NBuilder;
using FluentAssertions;
using NUnit.Framework;

namespace Code.Exercise.April2018.Tests
{
  [TestFixture]
  public class UserServiceTests
  {
    private IUserService _userService;
    private FakeDataContext _dataContext;
    private IActivationHelper _activationHelper;

    [SetUp]
    public void Setup()
    {
      _dataContext = new FakeDataContext(GetHashList(10));
      _activationHelper = new ActivationHelper();
      _userService = new UserService(_dataContext, _activationHelper);
    }

    [Test]
    public async Task GetContactEmailParamsAsync_GivenRealDataContext_ShouldReadRecordsFromFileAndReturnThoseResults()
    {
      //arrange
      var dataContext = new DataContext();
      var userService = new UserService(dataContext, _activationHelper);
      var hashes = await dataContext.GetContactHashes();
      var dataCount = hashes.Count;
      
      var collection = GetContactCollection(dataCount);

      // action
      var result = await userService.GetContactEmailParamsAsync(collection);

      // assert
      var resultData = result as MessageData[] ?? result.ToArray();
      resultData.Length.Should().Be(dataCount);
      for (var resultIndex = 0; resultIndex < resultData.Length; resultIndex++)
      {
        resultData[resultIndex].ActionUri.Should().Contain(hashes[resultIndex]);
      }
    }

    [Test]
    public async Task GetContactEmailParamsAsync_GivenNoContactCollection_ShouldReturnNothing()
    {
      // action
      var result = await _userService.GetContactEmailParamsAsync(null);

      // assert
      result.Should().BeEmpty();
    }

    [Test]
    public async Task GetContactEmailParamsAsync_GivenContactWithEmail_ShouldReturnRecipientAddress()
    {
      //arrange
      var collection = GetContactCollection(1);
      var firstContact = collection[0];

      // action
      var result = await _userService.GetContactEmailParamsAsync(collection);

      // assert
      result.First().RecipientAddress.Should().Be(firstContact.Email);
    }

    [Test]
    public async Task GetContactEmailParamsAsync_GivenContactFirstAndLastName_ShouldReturnRecipientName()
    {
      //arrange
      var collection = GetContactCollection(1);
      var firstContact = collection[0];

      // action
      var result = await _userService.GetContactEmailParamsAsync(collection);

      // assert
      result.First().RecipientName.Should().Be($"{firstContact.FirstName} {firstContact.LastName}");
    }

    [Test]
    public async Task GetContactEmailParamsAsync_GivenContactWithNegativeId_ShouldReturnNothing()
    {
      //arrange
      var collection = GetContactCollection(1);
      var firstContact = collection[0];
      firstContact.Id = -10;

      // action
      var result = await _userService.GetContactEmailParamsAsync(collection);

      // assert
      result.Should().BeEmpty();
    }

    [Test]
    public async Task GetContactEmailParamsAsync_GivenContactWithIdHigherThanHashRecordcount_ShouldReturnNothing()
    {
      //arrange
      _dataContext.SetHashList(GetHashList(5));
      var collection = GetContactCollection(1);
      var firstContact = collection[0];
      firstContact.Id = 8;

      // action
      var result = await _userService.GetContactEmailParamsAsync(collection);

      // assert
      result.Should().BeEmpty();
    }

    [Test]
    public async Task GetContactEmailParamsAsync_Given100ContactsBut50Hashes_ShouldReturnOnly50Results()
    {
      //arrange
      _dataContext.SetHashList(GetHashList(50));
      var collection = GetContactCollection(100);

      // action
      var result = await _userService.GetContactEmailParamsAsync(collection);

      // assert
      result.Count().Should().Be(50);
    }

    [Test]
    public async Task GetContactEmailParamsAsync_Given50ContactsBut100Hashes_ShouldReturnOnly50Results()
    {
      //arrange
      _dataContext.SetHashList(GetHashList(100));
      var collection = GetContactCollection(50);

      // action
      var result = await _userService.GetContactEmailParamsAsync(collection);

      // assert
      result.Count().Should().Be(50);
    }

    [Test]
    public async Task GetContactEmailParamsAsync_Given1000ContactsAnd1000Hashes_ShouldReturn1000Results()
    {
      //arrange
      _dataContext.SetHashList(GetHashList(1000));
      var collection = GetContactCollection(1000);

      // action
      var result = await _userService.GetContactEmailParamsAsync(collection);

      // assert
      result.Count().Should().Be(1000);
    }

    #region Private Methods

    private static List<Contact> GetContactCollection(int size)
    {
      return Builder<Contact>.CreateListOfSize(size)
        .All()
        .With(x => x.Id--)
        .Build()
        .ToList();
    }

    private static List<string> GetHashList(int size)
    {
      var generator = new RandomGenerator();
      return Enumerable.Range(0, size).Select(el => generator.NextString(10, 10)).ToList();
    }

    #endregion
  }
}
