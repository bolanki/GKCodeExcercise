﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Code.Exercise.April2018.Core.Interfaces;
using Code.Exercise.April2018.Models;

namespace Code.Exercise.April2018.Core
{
  public class UserService : IUserService
  {
    private readonly IDataContext _dataContext;
    private readonly IActivationHelper _activationHelper;

    public UserService(IDataContext dataContext, IActivationHelper activationHelper)
    {
      _dataContext = dataContext;
      _activationHelper = activationHelper;
}

    public async Task<IEnumerable<MessageData>> GetContactEmailParamsAsync(IEnumerable<Contact> contactCollection)
    {
      var contactHashes = await GetContactHashes();
      var contactArray = contactCollection?.ToArray();
      if (ValidateDataForContactMessages(contactArray, contactHashes))
      {
        return await PopulateContactParams(contactArray, contactHashes);
      }
      return new List<MessageData>();
    }

    protected virtual async Task<string> GetActionUriAsync(string contactHash)
    {
      return await _activationHelper.GetActionUri(contactHash);
    }

    #region Private Methods

    private async Task<List<string>> GetContactHashes()
    {
      return await _dataContext.GetContactHashes();
    }

    private async Task<MessageData> BuildMessageDataForContact(Contact contact, string contactHash)
    {
      return new MessageData
      {
        RecipientAddress = contact.Email,
        RecipientName = $"{contact.FirstName} {contact.LastName}",
        ActionUri = await GetActionUriAsync(contactHash)
      };
    }

    private async Task<IEnumerable<MessageData>> PopulateContactParams(IEnumerable<Contact> collection, List<string> contactHashes)
    {
      var resultList = new List<MessageData>();

      foreach (var contact in collection)
      {
        if (contact.Id > -1 && !(contact.Id >= contactHashes.Count))
        {
          resultList.Add(await BuildMessageDataForContact(contact, contactHashes[contact.Id]));
        }
      }

      return resultList.AsEnumerable();
    }

    private static bool ValidateDataForContactMessages(IEnumerable<Contact> contactCollection, List<string> contactHashes)
    {
      return contactHashes != null && contactHashes.Any() && contactCollection != null && contactCollection.Any();
    }

    #endregion
  }
}
