﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Code.Exercise.April2018.Core.Interfaces;

namespace Code.Exercise.April2018.Core
{
  public class DataContext : IDataContext
  {
    public Task<List<string>> GetContactHashes()
    {
      return Task.FromResult(File.ReadAllLines("tokens.txt").ToList());
    }
  }
}
